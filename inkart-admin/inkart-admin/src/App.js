import { Switch, Route } from "react-router-dom";
import HomePage from "./pages/homepage/HomePage";
// import Header from "./components/header/Header";
import SellerManagementPage from "./pages/management-pages/user-management/SellerManagementPage";
import CustomerManagementPage from "./pages/management-pages/user-management/CustomerManagementPage";
import ProductManagementPage from "./pages/management-pages/product-management/ProductManagementPage";
import OrderManagementPage from "./pages/management-pages/product-management/OrderManagementPage";
import CategoryManagementPage from "./pages/management-pages/category-management/CategoryManagementPage";
import SubCategoryManagementPage from "./pages/management-pages/category-management/SubCategoryManagementPage";
import ProductDetail from "./components/management-components/product-m-components/products-component/ProductDetail";
import Login from "./pages/login/Login";
import ProductEdit from "./components/management-components/product-m-components/products-component/ProductEdit";
import Edit from "./components/management-components/product-m-components/products-component/null/Edit";

function App() {
  return (
    <div className="App">
      {/* <Header /> */}
      <Switch>
        <Route exact path="/" component={Login} />
        <Route
          exact
          path="/users/seller-manage"
          component={SellerManagementPage}
        />
        <Route
          exact
          path="/users/customer-manage"
          component={CustomerManagementPage}
        />
        <Route
          exact
          path="/products/product-manage"
          component={ProductManagementPage}
        />
        <Route
          exact
          path="/products/product-detail/:productID"
          component={ProductDetail}
        />
        <Route exact path="/product/edit/:productID" component={ProductEdit} />
        <Route
          exact
          path="/products/order-manage"
          component={OrderManagementPage}
        />
        <Route
          exact
          path="/category/category-manage"
          component={CategoryManagementPage}
        />
        <Route
          exact
          path="/category/subcategory-manage"
          component={SubCategoryManagementPage}
        />
        <Route exact path="/home" component={HomePage} />
        <Route exact path="/product/edit" component={Edit} />
      </Switch>
    </div>
  );
}

export default App;
