import React from "react";
import Header from "../../../components/header/Header";
import SubCategoryTable from "../../../components/management-components/category-m-components/subcategory-component/SubCategoryTable";

export default function SubCategoryManagementPage() {
  return (
    <div>
      <Header />
      <SubCategoryTable />
    </div>
  );
}
