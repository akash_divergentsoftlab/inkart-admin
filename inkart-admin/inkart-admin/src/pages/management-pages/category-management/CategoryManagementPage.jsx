import React from "react";
import Header from "../../../components/header/Header";
import CategoryTable from "../../../components/management-components/category-m-components/category-component/CategoryTable";

export default function CategoryManagementPage() {
  return (
    <div>
      <Header />
      <CategoryTable />
    </div>
  );
}
