import React from "react";
import Header from "../../../components/header/Header";
import ProductTable from "../../../components/management-components/product-m-components/products-component/ProductTable";

export default function ProductManagementPage() {
  return (
    <div>
      <Header/>
        <ProductTable />
    </div>
  );
}
