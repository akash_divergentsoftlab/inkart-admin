import React, { Component } from "react";
import { URL } from "../../CONSTANT_URL";
import { withRouter } from "react-router-dom";
import "./Login.styles.scss";

class Login extends Component {
  constructor() {
    super();
    this.state = {
      username: null,
      password: null,
      isloggedIn: false,
    };
  }

  login() {
    fetch(`${URL}/authenticate`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(this.state),
    }).then((response) => {
      response.json().then((result) => {
        console.warn("result", result);
        localStorage.setItem("token", result.id_token);
        this.setState({ isloggedIn: true });
        this.props.history.push("/home");
      });
    });
  }

  render() {
    return (
      <div>
        <div className="login-form">
          <p class="login-text">
            <span class="fa-stack fa-lg">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fa fa-lock fa-stack-1x"></i>
            </span>
          </p>
          <p class="login-text">Admin Login </p>
          <input
            type="text"
            class="login-username"
            autofocus="true"
            required="true"
            placeholder="Username"
            onChange={(event) => {
              this.setState({
                username: event.target.value,
              });
            }}
          />
          <input
            type="password"
            class="login-password"
            required="true"
            placeholder="Password"
            onChange={(event) => {
              this.setState({
                password: event.target.value,
              });
            }}
          />
          <input
            type="submit"
            name="Login"
            value="Login"
            class="login-submit"
            onClick={() => {
              this.login();
            }}
          />
          <div class="underlay-photo"></div>
          <div class="underlay-black"></div>
        </div>
      </div>
    );
  }
}

export default withRouter(Login);
