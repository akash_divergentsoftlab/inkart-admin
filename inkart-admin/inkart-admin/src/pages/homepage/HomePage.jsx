import React from "react";
import Header from "../../components/header/Header";
// import BrandManagement from "../../components/homepagecomponents/BrandManagement";
import CategoryManagement from "../../components/homepagecomponents/CategoryManagement";
import ProductManagement from "../../components/homepagecomponents/ProductManagement";
import UserManagement from "../../components/homepagecomponents/UserManagement";
import SideBar from "../../components/sidebar/SideBar";
import "./homepage.scss";

export default function HomePage() {
  return (
    <div>
      <Header />
      <main>
        <header>
          <SideBar />
        </header>
        <br />
        <br />
        <div className="allCatBoxes" style={{margin: "46px 6px 0px 19px"}}>
          <div className="row">
            <div className="col s8">
              <UserManagement />
            </div>

            <div className="col s6">
              <ProductManagement />
            </div>

            <div className="col s6">
              <CategoryManagement />
              <div className="col s6">{/* <BrandManagement /> */}</div>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
}
