import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import { useParams } from "react-router";
import { URL } from "../../../../CONSTANT_URL";
import "./productManagement.scss";
import Header from "../../../header/Header";

const useStyles = makeStyles((theme) => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

export default function ProductDetail() {
  const classes = useStyles();
  const [productDetail, setProductDetail] = useState([]);
  const { productID } = useParams();
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const getProductDetail = () =>
    fetch(`${URL}/product/find/${productID}`).then((res) => res.json());

  useEffect(() => {
    getProductDetail().then((productDetail) => setProductDetail(productDetail));
    // eslint-disable-next-line
  }, []);
  // console.log(productDetail);

  return (
    <div>
      <Header />
      <br />
      <br />
      <br />
      <h2>Product Detail: </h2>
      <hr />
      <div className="product-detail-page">
        <table>
          <tr>
            <th>Field</th>
            <th>Value</th>
          </tr>
          <tr>
            <td>ID:</td>
            <td>{productDetail.productId}</td>
          </tr>
          <tr>
            <td>name:</td>
            <td>{productDetail.productName}</td>
          </tr>
          <tr>
            <td>image:</td>
            <td>
              {productDetail.imageUrl}{" "}
              <button type="button" onClick={handleOpen}>
                View Image
              </button>
            </td>
          </tr>
          <tr>
            <td>description:</td>
            <td>{productDetail.description}</td>
          </tr>
          <tr>
            <td>colour:</td>
            <td>{productDetail.colour}</td>
          </tr>
          <tr>
            <td>brand name:</td>
            <td>{productDetail.brandName}</td>
          </tr>
          <tr>
            <td>price:</td>
            <td>{productDetail.price}</td>
          </tr>
          <tr>
            <td>created date:</td>
            <td>{productDetail.createdAt}</td>
          </tr>
          <tr>
            <td>size:</td>
            <td>{productDetail.productSize}</td>
          </tr>
          <tr>
            <td>quantity:</td>
            <td>{productDetail.quantity}</td>
          </tr>
          <tr>
            <td>sub category name:</td>
            <td>{productDetail.subCategoryName}</td>
          </tr>
        </table>
      </div>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div className={classes.paper}>
            <h3 id="transition-modal-title">
              File name: {productDetail.imageUrl}{" "}
            </h3>
            <p id="transition-modal-description">
              <img
                className="imgg"
                src={productDetail.imageUrl}
                alt={productDetail.productName}
              />
            </p>
          </div>
        </Fade>
      </Modal>
    </div>
  );
}
