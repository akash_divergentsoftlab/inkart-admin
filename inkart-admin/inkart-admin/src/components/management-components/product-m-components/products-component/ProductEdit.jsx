import React, { useState, useEffect } from "react";
import "./ProductEdit.styles.scss";

const ProfileEdit = (props) => {
  const [products, setProduct] = useState(props.currentProducts);

  const handleInputChange = (event) => {
    const { name, value } = event.target;

    setProduct({ ...products, [name]: value });
  };
  useEffect(() => {
    setProduct(props.currentProducts);
  }, [props]);

  return (
    <form
      onSubmit={(event) => {
        event.preventDefault();

        props.updateProducts(products.id, products);
      }}
    >
      <label>Product Name</label>
      <input
      style={{height:"13px"}}
      className="productEditInout"
        type="text"
        name="productNames"
        value={products.productNames}
        onChange={handleInputChange}
      />
      <label>Brand Name</label>
      <input
      style={{height:"13px"}}
        type="text"
        name="brandNames"
        value={products.brandNames}
        onChange={handleInputChange}
      />
      <label>Colour</label>
      <input
        type="text"
        style={{height:"13px"}}
        name="colours"
        value={products.colours}
        onChange={handleInputChange}
      />
      <label>Image URL: </label>
      <input
        type="text"
        name="imageUrls"
        style={{height:"13px"}}
        value={products.imageUrls}
        onChange={handleInputChange}
      />
      <label>Price: </label>
      <input
        type="text"
        style={{height:"13px"}}
        name="prices"
        value={products.prices}
        onChange={handleInputChange}
      />
      <label>Product Size: </label>
      <input
        type="text"
        name="productSizes"
        style={{height:"13px"}}
        value={products.productSizes}
        onChange={handleInputChange}
      />
      <label>Quantity</label>
      <input
        type="text"
        name="quantitys"
        style={{height:"13px"}}
        value={products.quantitys}
        onChange={handleInputChange}
      />
      <label>Description</label>
      <input
        type="text"
        name="descriptions"
        style={{height:"13px"}}
        value={products.descriptions}
        onChange={handleInputChange}
      />
      <label>Sub Category</label>
      <input
        type="text"
        name="subCategoryNames"
        style={{height:"13px"}}
        value={products.subCategoryNames}
        onChange={handleInputChange}
      />
      <label>Created at</label>
      <input
        type="text"
        name="createdAts"
        style={{height:"13px"}}
        value={products.createdAts}
        onChange={handleInputChange}
      />
     
      <button>Update user</button>
      <button
        onClick={() => props.setEditing(false)}
        className="button muted-button"
      >
        Cancel
      </button>
    </form>
  );
};

export default ProfileEdit;
