import React, { useEffect, useState } from "react";
import AddUserForm from "./AddUserForm";
import EditUserForm from "./EditUserForm";
import axios from "axios";
import { URL } from "../../../../../CONSTANT_URL";
import { makeStyles } from "@material-ui/core";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import Header from "../../../../header/Header";

const useStyles = makeStyles((theme) => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

export default function Edit() {
  const [productsData, setproductsData] = useState([]);
  const [products, setProducts] = useState(productsData);
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const getProduct = () => {
    axios.get(`${URL}/product`).then((response) => {
      console.log(response);
      const myproducts = response.data;
      setProducts(myproducts);
    });
  };

  useEffect(() => getProduct(), []);

  const [editing, setEditing] = useState(false);
  const initialFormState = {
    productId: null,
    productName: "",
    brandName: "",
    subCategoryName: "",
    price: "",
    imageUrl: "",
    description: "",
    quantity: "",
    productSize: "",
    colour: "",
    createdAt: "",
  };
  const [currentProducts, setcurrentProducts] = useState(initialFormState);

  const editRow = (row) => {
    setEditing(true);
    console.log(row);
    setcurrentProducts({
      id: row.productId,
      brandName: row.brandName,
      productName: row.productName,
      productSize: row.productSize,
      colour: row.colour,
      createdAt: row.createdAt,
      description: row.description,
      imageUrl: row.imageUrl,
      price: row.price,
      quantity: row.quantity,
      subCategoryName: row.subCategoryName,
    });
  };

  const updateProducts = (id, updatedProducts) => {
    setEditing(false);

    setProducts(
      products.map((row) => (row.productId === id ? updatedProducts : row))
    );
  };
  return (
    <div>
      <Header />
      <br />
      <br />
      <br />
      <div className="container">
        <h1>Update Products: </h1>
        <div className="flex-row">
          <div className="flex-large">
            {editing ? (
              <div>
                <Modal
                  aria-labelledby="transition-modal-title"
                  aria-describedby="transition-modal-description"
                  className={classes.modal}
                  open={open}
                  onClose={handleClose}
                  closeAfterTransition
                  BackdropComponent={Backdrop}
                  BackdropProps={{
                    timeout: 500,
                  }}
                >
                  <Fade in={open}>
                    <div className={classes.paper}>
                      <EditUserForm
                        editing={editing}
                        setEditing={setEditing}
                        currentProducts={currentProducts}
                        updateProducts={updateProducts}
                      />
                    </div>
                  </Fade>
                </Modal>
              </div>
            ) : (
              <div></div>
            )}
          </div>
          <div className="flex-large">
            <table>
              <thead>
                <tr>
                  <th>Product ID: </th>
                  <th>Product Name: </th>
                  <th>Brand Name: </th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                {products.length > 0 ? (
                  products.map((row) => (
                    <tr key={row.productId}>
                      <td>{row.productId}</td>
                      <td>{row.productName}</td>
                      <td>{row.brandName}</td>
                      <td>
                        <button
                          onClick={() => {
                            editRow(row);
                            handleOpen();
                          }}
                          className="button muted-button"
                        >
                          Edit
                        </button>
                      </td>
                    </tr>
                  ))
                ) : (
                  <tr>
                    <td colSpan={10}>No products</td>
                  </tr>
                )}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
}
