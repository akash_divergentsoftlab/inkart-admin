import React, { useState, useEffect } from "react";

const EditUserForm = (props) => {
  const [products, setProduct] = useState(props.currentProducts);

  const handleInputChange = (event) => {
    const { name, value } = event.target;

    setProduct({ ...products, [name]: value });
  };
  useEffect(() => {
    setProduct(props.currentProducts);
  }, [props]);

  return (
    <div className="editProductForm" style={{ margin: "5px 327px 22px 142px" }}>
      <h1>Edit Products</h1>
      <hr />
      <form
        onSubmit={(event) => {
          event.preventDefault();

          props.updateProducts(products.id, products);
        }}
      >
        <label>Product Name: </label>
        <input
          style={{height: "21px"}}
          type="text"
          name="productName"
          value={products.productName}
          onChange={handleInputChange}
        />
        <label>Brand Name: </label>
        <input
          style={{height: "21px"}}
          type="text"
          name="brandName"
          value={products.brandName}
          onChange={handleInputChange}
        />
        <label>Sub Category: </label>
        <input
          style={{height: "21px"}}
          type="text"
          name="subCategoryName"
          value={products.subCategoryName}
          onChange={handleInputChange}
        />
        <label>Price: </label>
        <input
          style={{height: "21px"}}
          type="text"
          name="price"
          value={products.price}
          onChange={handleInputChange}
        />
        <label>Image: </label>
        <input
        style={{height: "21px"}}
          type="text"
          name="imageUrl"
          value={products.imageUrl}
          onChange={handleInputChange}
        />
        <label>Description: </label>
        <input
        style={{height: "21px"}}
          type="text"
          name="description"
          value={products.description}
          onChange={handleInputChange}
        />
        <label>Quantity: </label>
        <input
        style={{height: "21px"}}
          type="text"
          name="quantity"
          value={products.quantity}
          onChange={handleInputChange}
        />
        <label>Product Size: </label>
        <input
        style={{height: "21px"}}
          type="text"
          name="productSize"
          value={products.productSize}
          onChange={handleInputChange}
        />
        <label>Colour: </label>
        <input
        style={{height: "21px"}}
          type="text"
          name="colour"
          value={products.colour}
          onChange={handleInputChange}
        />
        <label>Created At: </label>
        <input
        style={{height: "21px"}}
          type="text"
          name="createdAt"
          value={products.createdAt}
          onChange={handleInputChange}
        />
        <button>Update user</button>
        <button
          onClick={() => props.setEditing(false)}
          className="button muted-button"
        >
          Cancel
        </button>
      </form>
    </div>
  );
};

export default EditUserForm;
