import React, { useEffect, useState } from "react";

const EditSubCategoryForm = (props) => {
  const [subcategorys, setsubCategorys] = useState(props.currentSubCategory);

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setsubCategorys({ ...subcategorys, [name]: value });
  };
  useEffect(() => {
    setsubCategorys(props.currentSubCategory);
  }, [props]);

  return (
    <div className="EditCategoryForm">
      <h1>Edit Category: </h1>
      <form
        onSubmit={(event) => {
          event.preventDefault();
          props.updateCategory(subcategorys.id, subcategorys);
        }}
      >
        <label>Category Id:</label>
        <input
          type="text"
          name="categoryName"
          value={subcategorys.categoryName}
          onChange={handleInputChange}
        />
        <label>Sub Category Name:</label>
        <input
          type="text"
          name="subCategoryName"
          value={subcategorys.subCategoryName}
          onChange={handleInputChange}
        />
        <label>Category Name:</label>
        <input
          type="text"
          name="categoryName"
          value={subcategorys.categoryName}
          onChange={handleInputChange}
        />
        <button>Update</button>
        <button onClick={() => props.handleClose()}>Cancel</button>
      </form>
    </div>
  );
};

export default EditSubCategoryForm;
