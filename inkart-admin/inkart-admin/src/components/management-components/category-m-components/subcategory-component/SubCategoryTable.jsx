import React, { useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { useState } from "react";
import CategoryIcon from "@material-ui/icons/Category";
import { URL } from "../../../../CONSTANT_URL";
import { Backdrop, Button, Fade, Modal } from "@material-ui/core";
// import EditCategoryForm from "./EditCategoryForm";
import SpeedDial from "@material-ui/lab/SpeedDial";
import SpeedDialIcon from "@material-ui/lab/SpeedDialIcon";
import SpeedDialAction from "@material-ui/lab/SpeedDialAction";
import EditSubCategoryForm from "./EditSubCategoryForm";

const useStyles = makeStyles((theme) => ({
  table: {
    minWidth: 650,
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

export default function SubCategoryTable() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const [open3, setOpen3] = React.useState(false);
  const [subcategory, setSubCategory] = useState([]);
  const [hidden, setHidden] = React.useState(false);
  const [editing, setEditing] = useState(false);
  const initialFormState = {
    subCategoryId: null,
    subCategoryName: "",
    categoryName: "",
  };

  
  const [currentSubCategory, setcurrentSubCategory] = useState(initialFormState);
  
  const handleOpen = () => {
    setOpen(true);
  };
  
  const handleClose = () => {
    setOpen(false);
  };
  
  const handleOpen1 = () => {
    setOpen(true);
  };
  
  const handleClose1 = () => {
    setOpen(false);
  };
  
  const handleOpen3 = () => {
    setOpen3(true);
  };
  
  const handleClose3 = () => {
    setOpen3(false);
  };
  
  const actions = [{ icon: <CategoryIcon onClick={handleOpen3} />, name: "Add Category" }];


  const editRow = (row) => {
    setEditing(true);
    console.log(row);
    setcurrentSubCategory({
        subCategoryId: row.subCategoryId,
        subCategoryName: row.subCategoryName,
        categoryName: row.categoryName,
    });
  };

  const handleVisibility = () => {
    setHidden((prevHidden) => !prevHidden);
  };

  const updateSubCategory = (id, updateSubCategory) => {
    setEditing(false);

    setSubCategory(
        subcategory.map((row) => (row.productId === id ? updateSubCategory : row))
    );
  };

  const getCategory = () => fetch(`${URL}/sub_category`).then((res) => res.json());

  useEffect(() => {
    getCategory().then((subcategory) => setSubCategory(subcategory));
  }, []);

  return (
    <div
      className="categoryTable"
      style={{ width: "43%", margin: "0px 0px 0px 426px" }}
    >
      <br />
      <br />
      <br />
      <h1>Category Table: </h1>
      {editing ? (
        <div>
          <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={classes.modal}
            open={open}
            onClose={handleClose}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
              timeout: 500,
            }}
          >
            <Fade in={open}>
              <div className={classes.paper}>
                <EditSubCategoryForm
                  editing={editing}
                  handleClose={handleClose}
                  setEditing={setEditing}
                  currentSubCategory={currentSubCategory}
                  updateSubCategory={updateSubCategory}
                />
              </div>
            </Fade>
          </Modal>
        </div>
      ) : (
        <div></div>
      )}
      <TableContainer component={Paper}>
        <Table
          className={classes.table}
          size="small"
          aria-label="a dense table"
        >
          <TableHead>
            <TableRow>
              <TableCell>Category ID: </TableCell>
              <TableCell align="right">Catgory name: </TableCell>
              <TableCell align="right">Category Name: </TableCell>
              <TableCell align="right">Operations</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {subcategory.map((row) => (
              <TableRow key={row.subCategoryId}>
                <TableCell component="th" scope="row">
                  {row.subCategoryId}
                </TableCell>
                <TableCell align="right">{row.subCategoryName}</TableCell>
                <TableCell align="right">{row.categoryName}</TableCell>
                <TableCell align="right">
                  <button
                    onClick={() => {
                      editRow(row);
                      handleOpen();
                    }}
                  >
                    Edit
                  </button>
                  <button>Delete</button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <Button onClick={handleVisibility}></Button>
      <Backdrop open={open} />
      <div className="speedDial" style={{marginLeft: "70rem"}}>
        <SpeedDial
          ariaLabel="SpeedDial tooltip example"
          className={classes.speedDial}
          hidden={hidden}
          icon={<SpeedDialIcon />}
          onClose={handleClose1}
          onOpen={handleOpen1}
          open={open}
        >
          {actions.map((action) => (
            <SpeedDialAction
              key={action.name}
              icon={action.icon}
              tooltipTitle={action.name}
              tooltipOpen
              onClick={handleClose1}
              style={{ fontWeight: "bolder" }}
            />
          ))}
        </SpeedDial>
      </div>
      {/* add form modal starts here  */}
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open3}
        onClose={handleClose3}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open3}>
          <div className={classes.paper}>
            <h2 id="transition-modal-title">Transition modal 2</h2>
            <p id="transition-modal-description">react-transition-group animates me.</p>
          </div>
        </Fade>
      </Modal>
      {/* add modal ends here */}
    </div>
  );
}
