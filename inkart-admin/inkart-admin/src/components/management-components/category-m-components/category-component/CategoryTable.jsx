import React, { useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { useState } from "react";
import CategoryIcon from "@material-ui/icons/Category";
import { URL } from "../../../../CONSTANT_URL";
import { Backdrop, Button, Fade, Modal } from "@material-ui/core";
import EditCategoryForm from "./EditCategoryForm";
import SpeedDial from "@material-ui/lab/SpeedDial";
import SpeedDialIcon from "@material-ui/lab/SpeedDialIcon";
import SpeedDialAction from "@material-ui/lab/SpeedDialAction";

const useStyles = makeStyles((theme) => ({
  table: {
    minWidth: 650,
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

export default function CategoryTable() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const [open3, setOpen3] = React.useState(false);
  const [category, setCategory] = useState([]);
  const [hidden, setHidden] = React.useState(false);
  const [editing, setEditing] = useState(false);
  const [categoryId, setcategoryId] = useState("");
  const [categoryName, setcategoryName] = useState("");
  const [categoryMain, setCategoryMain] = useState([]);

  const initialFormState = {
    categoryId: null,
    categoryName: "",
  };

  // async function newCategoryItem() {
  //   let item = { categoryId, categoryName };
  //   console.log(item);

  //   let result = await fetch("http://localhost:9000/api/category/save", {
  //     method: "POST",
  //     body: JSON.stringify(item),
  //     headers: {
  //       "Content-Type": "application/json",
  //       Accept: "application/json",
  //     },
  //   });
  //   result = await result.json();
  //   console.warn("result", result);
  // }

  const handleSubmit = (e) => {
    e.preventDefault();
    console.table(e);

    const URLS = `http://localhost:9000/api/category/save`;
    const token = localStorage.getItem('token');

    const bodyData = {
      categoryId: categoryId,
      categoryName: categoryName,
    };

    fetch(URLS, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Authorization" : `Bearer ${token}`
      },
      body: JSON.stringify(bodyData),
    })
      .then((res) => res.json())
      .then((res) => {
        console.log(res);
        setCategoryMain(res);
      });
  };

  const [currentCategory, setcurrentCategory] = useState(initialFormState);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleOpen1 = () => {
    setOpen(true);
  };

  const handleClose1 = () => {
    setOpen(false);
  };
  const handleOpen3 = () => {
    setOpen3(true);
  };

  const handleClose3 = () => {
    setOpen3(false);
  };

  const actions = [
    { icon: <CategoryIcon onClick={handleOpen3} />, name: "Add Category" },
  ];

  const editRow = (row) => {
    setEditing(true);
    console.log(row);
    setcurrentCategory({
      categoryId: row.categoryId,
      categoryName: row.categoryName,
    });
  };

  const handleVisibility = () => {
    setHidden((prevHidden) => !prevHidden);
  };

  const updateCategory = (id, updateCategory) => {
    setEditing(false);

    setCategory(
      category.map((row) => (row.productId === id ? updateCategory : row))
    );
  };

  const getCategory = () => fetch(`${URL}/category`).then((res) => res.json());

  useEffect(() => {
    getCategory().then((category) => setCategory(category));
  }, []);

  return (
    <div
      className="categoryTable"
      style={{ width: "43%", margin: "0px 0px 0px 426px" }}
    >
      <br />
      <br />
      <br />
      <h1>Category Table: </h1>
      <Button onClick={handleVisibility}></Button>
      <Backdrop open={open} />
      <div
        className="speedDial"
        style={{ marginLeft: "70rem", marginTop: "-60px" }}
      >
        <SpeedDial
          ariaLabel="SpeedDial tooltip example"
          className={classes.speedDial}
          hidden={hidden}
          icon={<SpeedDialIcon />}
          onClose={handleClose1}
          onOpen={handleOpen1}
          direction="down"
          open={open}
        >
          {actions.map((action) => (
            <SpeedDialAction
              key={action.name}
              icon={action.icon}
              tooltipTitle={action.name}
              tooltipOpen
              onClick={handleClose1}
              style={{ fontWeight: "bolder" }}
            />
          ))}
        </SpeedDial>
      </div>
      {editing ? (
        <div>
          <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={classes.modal}
            open={open}
            onClose={handleClose}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
              timeout: 500,
            }}
          >
            <Fade in={open}>
              <div className={classes.paper}>
                <EditCategoryForm
                  editing={editing}
                  handleClose={handleClose}
                  setEditing={setEditing}
                  currentCategory={currentCategory}
                  updateCategory={updateCategory}
                />
              </div>
            </Fade>
          </Modal>
        </div>
      ) : (
        <div></div>
      )}
      <TableContainer component={Paper}>
        <Table
          className={classes.table}
          size="small"
          aria-label="a dense table"
        >
          <TableHead>
            <TableRow>
              <TableCell>Category ID: </TableCell>
              <TableCell align="right">Catgory name: </TableCell>
              <TableCell align="right">Operations</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {category.map((row) => (
              <TableRow key={row.categoryId}>
                <TableCell component="th" scope="row">
                  {row.categoryId}
                </TableCell>
                <TableCell align="right">{row.categoryName}</TableCell>
                <TableCell align="right">
                  <button
                    onClick={() => {
                      editRow(row);
                      handleOpen();
                    }}
                  >
                    Edit
                  </button>
                  <button>Delete</button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>

      {/* add form modal starts here  */}
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open3}
        onClose={handleClose3}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open3}>
          <div className={classes.paper}>
            <h1>Add Category:</h1>
            <div className="addCategoryName">
              <form  onSubmit={handleSubmit} >
                <label>Category ID: </label>
                <input
                  type="text"
                  placeholder="category ID"
                  disabled
                  value={categoryId}
                  onChange={(e) => setcategoryId(e.target.value)}
                  style={{ cursor: "not-allowed" }}
                />
                <label>Category Name: </label>
                <input
                  type="text"
                  value={categoryName}
                  onChange={(e) => setcategoryName(e.target.value)}
                  placeholder="category Name"
                />
                <button  type="submit">
                  Add
                </button>
                <button onClick={handleClose3} type="submit">
                  Cancel
                </button>
              </form>
            </div>
          </div>
        </Fade>
      </Modal>
      {/* add modal ends here */}
    </div>
  );
}
