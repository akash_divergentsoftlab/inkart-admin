import React, { useEffect, useState } from "react";

const EditCategoryForm = (props) => {
  const [categorys, setCategorys] = useState(props.currentCategory);

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setCategorys({ ...categorys, [name]: value });
  };
  useEffect(() => {
    setCategorys(props.currentCategory);
  }, [props]);

  return (
    <div className="EditCategoryForm">
      <h1>Edit Category: </h1>
      <form
        onSubmit={(event) => {
          event.preventDefault();
          props.updateCategory(categorys.id, categorys);
        }}
      >
        <label>Category Id:</label>
        <input
          type="text"
          name="categoryId"
          value={categorys.categoryId}
          onChange={handleInputChange}
        />
        <label>Category Name:</label>
        <input
          type="text"
          name="categoryName"
          value={categorys.categoryName}
          onChange={handleInputChange}
        />
        <button>Update</button>
        <button onClick={() => props.handleClose()}>Cancel</button>
      </form>
    </div>
  );
};

export default EditCategoryForm;
