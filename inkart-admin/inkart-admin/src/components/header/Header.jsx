import React from "react";
import { Container, Nav, Navbar, NavDropdown } from "react-bootstrap";

export default function Header() {
  return (
    <div>
      <Navbar
        className="fixed"
        collapseOnSelect
        expand="lg"
        bg="dark"
        variant="dark"
        style={{ zIndex: "10", position: "fixed" }}
      >
        <Container>
          <Navbar.Brand href="/home">Inkart-Admin-Panel</Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="me-auto">
              <Nav.Link href="/home">Home</Nav.Link>
              <Nav.Link href="/home">Dashboard</Nav.Link>
              <NavDropdown title="Admin-features" id="collasible-nav-dropdown">
                <NavDropdown.Item href="#action/3.1">
                  See registered users
                </NavDropdown.Item>
                <NavDropdown.Item href="#action/3.2">
                  Create operations
                </NavDropdown.Item>
                <NavDropdown.Item href="#action/3.2">
                  Update operations
                </NavDropdown.Item>
                <NavDropdown.Item href="#action/3.2">
                  Retieve operations
                </NavDropdown.Item>
                <NavDropdown.Item href="#action/3.2">
                  Delete operations
                </NavDropdown.Item>

                <NavDropdown.Divider />
                <NavDropdown.Item href="#action/3.4">
                  All Crud Operations
                </NavDropdown.Item>
              </NavDropdown>
            </Nav>
            <Nav>
              <Nav.Link href="#deets">Current Admin</Nav.Link>
              <Nav.Link eventKey={2} href="#memes">
                Logout
              </Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </div>
  );
}
