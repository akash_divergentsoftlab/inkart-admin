import React from 'react'
import { Link } from 'react-router-dom'

export default function SideBar() {
    return (
        <div>
              <ul
        id="slide-out"
        className="side-nav fixed z-depth-2"
        style={{ marginTop: "63px" }}
      >
        <ul className="collapsible" data-collapsible="accordion" style={{marginLeft: "-51px"}}>
          <li id="dash_users" >
            <div
              id="dash_users_header"
              className="collapsible-header waves-effect"
            >
              <b>Users</b>
            </div>
            <div id="dash_users_body" className="collapsible-body">
              <ul>
                <li id="users_seller">
                  <Link
                    className="waves-effect"
                    style={{ textDecoration: "none" }}
                    to="/users/seller-manage"
                  >
                    Seller
                  </Link>
                </li>

                <li id="users_customer">
                  <Link
                    className="waves-effect"
                    style={{ textDecoration: "none" }}
                    to="/users/customer-manage"
                  >
                    Customer
                  </Link>
                </li>
              </ul>
            </div>
          </li>

          <li id="dash_products">
            <div
              id="dash_products_header"
              className="collapsible-header waves-effect"
            >
              <b>Products</b>
            </div>
            <div id="dash_products_body" className="collapsible-body">
              <ul>
                <li id="products_product">
                  <Link
                    className="waves-effect"
                    style={{ textDecoration: "none" }}
                    to="/products/product-manage"
                  >
                    Products
                  </Link>
                  <Link
                    className="waves-effect"
                    style={{ textDecoration: "none" }}
                    to="/products/order-manage"
                  >
                    Orders
                  </Link>
                </li>
              </ul>
            </div>
          </li>

          <li id="dash_categories">
            <div
              id="dash_categories_header"
              className="collapsible-header waves-effect"
            >
              <b>Categories</b>
            </div>
            <div id="dash_categories_body" className="collapsible-body">
              <ul>
                <li id="categories_category">
                  <Link
                    className="waves-effect"
                    style={{ textDecoration: "none" }}
                    to="/category/category-manage"
                  >
                    Category
                  </Link>
                </li>

                <li id="categories_sub_category">
                  <Link
                    className="waves-effect"
                    style={{ textDecoration: "none" }}
                    to="/category/subcategory-manage"
                  >
                    Sub Category
                  </Link>
                </li>
              </ul>
            </div>
          </li>

          {/* <li id="dash_brands">
            <div
              id="dash_brands_header"
              className="collapsible-header waves-effect"
            >
              <b>Brands</b>
            </div>
            <div id="dash_brands_body" className="collapsible-body">
              <ul>
                <li id="brands_brand">
                  <a
                    className="waves-effect"
                    style={{ textDecoration: "none" }}
                    href="#!"
                  >
                    Brand
                  </a>
                </li>

                <li id="brands_sub_brand">
                  <a
                    className="waves-effect"
                    style={{ textDecoration: "none" }}
                    href="#!"
                  >
                    Sub Brand
                  </a>
                </li>
              </ul>
            </div>
          </li> */}
        </ul>
      </ul>

        </div>
    )
}
