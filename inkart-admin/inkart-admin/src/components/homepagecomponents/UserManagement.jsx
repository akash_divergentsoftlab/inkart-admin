import React from "react";
import { Link } from "react-router-dom";

export default function UserManagement() {
  return (
    <div>
      <div
        style={{
          padding: "27px",
          height: "280px",
          width: "381px",
          marginLeft: "45px",
        }}
        align="center"
        className="card"
      >
        <div className="row">
          <div className="left card-title">
            <b>User Management</b>
          </div>
        </div>

        <div className="row">
          <div className="boxContainer">
            <Link to="/users/seller-manage">
              <div
                style={{ padding: "30p" }}
                className="grey lighten-3 col s5 waves-effect"
              >
                <i className="indigo-text text-lighten-1 large material-icons">
                  person
                </i>
                <span className="indigo-text text-lighten-1">
                  <h5>Seller</h5>
                </span>
              </div>
            </Link>
            <div className="col s1">&nbsp;</div>
            <div className="col s1">&nbsp;</div>

            <Link to="/users/customer-manage">
              <div
                style={{ padding: "30p" }}
                className="grey lighten-3 col s5 waves-effect"
              >
                <i className="indigo-text text-lighten-1 large material-icons">
                  people
                </i>
                <span className="indigo-text text-lighten-1">
                  <h5>Customer</h5>
                </span>
              </div>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
}
