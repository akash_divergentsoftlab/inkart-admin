import React from "react";

export default function BrandManagement() {
  return (
    <div>
      <div style={{ padding: "35px" }} align="center" className="card">
        <div className="row">
          <div className="left card-title">
            <b>Brand Management</b>
          </div>
        </div>

        <div className="row">
          <a href="#!">
            <div
              style={{ padding: "30p" }}
              className="grey lighten-3 col s5 waves-effect"
            >
              <i className="indigo-text text-lighten-1 large material-icons">
                local_offer
              </i>
              <span className="indigo-text text-lighten-1">
                <h5>Brand</h5>
              </span>
            </div>
          </a>

          <div className="col s1">&nbsp;</div>
          <div className="col s1">&nbsp;</div>

          <a href="#!">
            <div
              style={{ padding: "30p" }}
              className="grey lighten-3 col s5 waves-effect"
            >
              <i className="indigo-text text-lighten-1 large material-icons">
                loyalty
              </i>
              <span className="indigo-text text-lighten-1">
                <h5>Sub Brand</h5>
              </span>
            </div>
          </a>
        </div>
      </div>
    </div>
  );
}
