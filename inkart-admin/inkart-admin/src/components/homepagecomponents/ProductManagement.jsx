import React from "react";
import { Link } from "react-router-dom";

export default function ProductManagement() {
  return (
    <div>
      <div
        style={{
          padding: "27px",
          height: "280px",
          width: "381px",
          marginLeft: "45px",
        }}
        align="center"
        className="card"
      >
        <div className="row">
          <div className="left card-title">
            <b>Product Management</b>
          </div>
        </div>
        <div className="row">
          <div className="boxContainer">
            <Link to="/products/product-manage">
              <div
                style={{ padding: "30p" }}
                className="grey lighten-3 col s5 waves-effect"
              >
                <i className="indigo-text text-lighten-1 large material-icons">
                  store
                </i>
                <span className="indigo-text text-lighten-1">
                  <h5>Product</h5>
                </span>
              </div>
            </Link>

            <div className="col s1">&nbsp;</div>
            <div className="col s1">&nbsp;</div>

            <Link to="/products/order-manage">
              <div
                style={{ padding: "30p" }}
                className="grey lighten-3 col s5 waves-effect"
              >
                <i className="indigo-text text-lighten-1 large material-icons">
                  assignment
                </i>
                <span className="indigo-text text-lighten-1">
                  <h5>Orders</h5>
                </span>
              </div>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
}
