import React from "react";
import { Link } from "react-router-dom";

export default function CategoryManagement() {
  return (
    <div>
      <div
        style={{
          padding: "27px",
          height: "280px",
          width: "390px",
          marginLeft: "33px",
        }}
        align="center"
        className="card"
      >
        <div className="row">
          <div className="left card-title">
            <b>Category Management</b>
          </div>
        </div>
        <div className="row">
          <div className="boxContainer">
            <Link to="/category/category-manage">
              <div
                style={{ padding: "30p" }}
                className="grey lighten-3 col s5 waves-effect"
              >
                <i className="indigo-text text-lighten-1 large material-icons">
                  view_list
                </i>
                <span className="indigo-text text-lighten-1">
                  <h5>Category</h5>
                </span>
              </div>
            </Link>
            <div className="col s1">&nbsp;</div>
            <div className="col s1">&nbsp;</div>

            <Link to="/category/subcategory-manage">
              <div
                style={{ padding: "30p" }}
                className="grey lighten-3 col s5 waves-effect"
              >
                <i className="indigo-text text-lighten-1 large material-icons">
                  view_list
                </i>
                <span className="truncate indigo-text text-lighten-1">
                  <h5>Sub Category</h5>
                </span>
              </div>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
}
